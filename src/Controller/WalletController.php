<?php

namespace App\Controller;

use App\Entity\Crypto;
use App\Entity\User;
use App\Entity\Wallet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WalletController extends AbstractController
{
    #[Route('/wallet/addAmount/{idUser}/{amount}', name: 'add_money_wallet')]
    public function addAmount(EntityManagerInterface $doctrine, $idUser, $amount): Response
    {

        $repoU = $doctrine->getRepository(User::class);
        $user = $repoU->find($idUser);

        $repoW = $doctrine->getRepository(Wallet::class);
        $wallet = $repoW->findOneBy(['user_wallet'=>$user]);
        $AmountActual = $wallet->getAmount();
        $wallet->setAmount($amount + $AmountActual);


        $doctrine->persist($wallet);
        $doctrine->flush();
        $this->addFlash('success', 'Más dinero en tu cuenta');
        return $this->redirect($this->generateUrl('see_user', array( 'id' => $idUser)));
    }

 #[Route('/wallet/addCrypto/{idUser}/{idCrypto}', name: 'add_crypto_wallet')]
    public function addCryptoToWallet(EntityManagerInterface $doctrine,$idUser, $idCrypto): Response
    {
        $repo = $doctrine->getRepository(Crypto::class);
        $crypto = $repo->find($idCrypto);

        $repoU = $doctrine->getRepository(User::class);
        $user = $repoU->find($idUser);

        $repoW = $doctrine->getRepository(Wallet::class);
        $wallet = $repoW->findOneBy(['user_wallet'=>$user]);
        $wallet->addCryptoInWallet($crypto);


        $doctrine->persist($wallet);
        $doctrine->flush();


        $this->addFlash('success', "Cripto tu cuenta");
        return $this->redirect($this->generateUrl('see_user', array( 'id' => $idUser)));
    }
}
