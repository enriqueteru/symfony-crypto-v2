<?php

namespace App\Controller;

use App\Entity\Crypto;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CryptoController extends AbstractController
{
    #[Route('/', name: 'app_crypto')]
    public function index(EntityManagerInterface $doctrine): Response
    {

        $repo = $doctrine->getRepository(Crypto::class);
        $cryptos = $repo->findAll();

        return $this->render('crypto/cryptoList.html.twig', [ 'cryptos' => $cryptos ]);
    }

    #[Route('add-url/crypto', name: 'add_url_crypto')]
    public function addUrl(EntityManagerInterface $doctrine): Response
    {

        $crypto = new Crypto();
        $crypto->setName('Oasis Network');
        $crypto->setLogoUrl('https://dynamic-assets.coinbase.com/f0e7bb761f5b88eb3b89da4d170d32cc5b961714e883868fa735115406457dbd9ddc7deed8428dffce01bf48dc656e24f64fd1c5489551748ef3917dc5ba70ad/asset_icons/bfaeca758204617fd9159f15de134f66e922a538b913d8d781bef4d98a25cec4.png');
        $crypto->setDescription('Oasis es una cadena de bloques de capa base, como Ethereum o Bitcoin , pero está construida desde cero, con confidencialidad de datos y ganancias de rendimiento de hasta 1,000 transacciones por segundo.');
        $crypto->setSymbol('ROSE');
        $crypto->setPrice(0.19352543 );
        $crypto->setRanking(6);

        $doctrine->persist($crypto);
        $doctrine->flush();

        return $this->json([
            'message' => 'Crypto Added To the DB!',
            'path' => 'src/Controller/CryptoController.php',
        ]);
    }
    #[Route('search/crypto/{id}', name: 'search_crypto')]
    public function searchCrypto(EntityManagerInterface $doctrine, $id): Response
    {

        $repo = $doctrine->getRepository(Crypto::class);
        $crypto = $repo->find($id);

        return $this->render('crypto/cryptoSearch.html.twig', [ 'crypto' => $crypto ]);
    }
}
